﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.OleDb;

namespace jurnal
{
    public partial class Form1 : Form
    {
        static OleDbConnection connection = new OleDbConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);
        static OleDbDataAdapter adapter;
        static DataSet ds1 = new DataSet();
        static string selecteddiscip;
        static bool updated;
        public Form1()
        {
            InitializeComponent();
            readds();
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            dateTimePicker3.Value = DateTime.Now;
        }

        private void tableupdate(DataGridView table1,string table)
        {
            updated = false;
            table1.Rows.Clear();
            foreach (DataRow row in ds1.Tables[table].Rows)
                table1.Rows.Add(row.ItemArray);
            updated = true;
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            tableupdate(dataGridView1,"Students");
        }

        private void tabPage3_Enter(object sender, EventArgs e)
        {
            tableupdate(dataGridView2,"Prepod");
        }

        private void readds()
        {
            ds1 = new DataSet();
            adapter = new OleDbDataAdapter("select KodStud,SFam,SName,Phone,Address from Students order by SFam,SName", connection);
            connection.Open();
            adapter.Fill(ds1,"Students");
            adapter = new OleDbDataAdapter("select * from Propuski", connection);
            adapter.Fill(ds1,"Propuski");
            adapter = new OleDbDataAdapter("select * from discip order by DName", connection);
            adapter.Fill(ds1, "Discip");
            adapter = new OleDbDataAdapter("select * from Prepod order by PFam,PName,POtch", connection);
            adapter.Fill(ds1, "Prepod");
            connection.Close();
            ds1.Relations.Add("fio", ds1.Tables["Students"].Columns["KodStud"], ds1.Tables["Propuski"].Columns["KodStud"]);
            comboBox1.DataSource = ds1.Tables["Discip"];
            comboBox1.DisplayMember = "DName";
            comboBox1.ValueMember = "KodDiscip";
            updatesavedchanges();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            adapter = new OleDbDataAdapter("select * from Propuski", connection);
            OleDbCommand update = new OleDbCommand("Update Propuski set Skipped = @Skipped, Valid = @Valid WHERE KodDiscip = @KodDiscip AND KodStud = @KodStud AND PDate = @PDate", connection);
            update.Parameters.Add("@Skipped", OleDbType.SmallInt, 1, "Skipped");
            update.Parameters.Add("@Valid", OleDbType.Boolean, 1, "Valid");
            update.Parameters.Add("@KodDiscip", OleDbType.SmallInt, 1, "KodDiscip");
            update.Parameters.Add("@KodStud", OleDbType.SmallInt, 1, "KodStud");
            update.Parameters.Add("@PDate", OleDbType.DBDate, 1, "PDate");
            adapter.UpdateCommand = update;
            OleDbCommand insert = new OleDbCommand("Insert into Propuski (KodDiscip,KodStud,PDate,Skipped,Valid) values (@KodDiscip,@KodStud,@PDate,@Skipped,@Valid)", connection);
            insert.Parameters.Add("@KodDiscip", OleDbType.SmallInt, 1, "KodDiscip");
            insert.Parameters.Add("@KodStud", OleDbType.SmallInt, 1, "KodStud");
            insert.Parameters.Add("@PDate", OleDbType.DBDate, 1, "PDate");
            insert.Parameters.Add("@Skipped", OleDbType.SmallInt, 1, "Skipped");
            insert.Parameters.Add("@Valid", OleDbType.Boolean, 1, "Valid");
            adapter.InsertCommand = insert;
            OleDbCommand delete = new OleDbCommand("delete from Propuski where Skipped = 0 OR Skipped is null", connection);
            adapter.DeleteCommand = delete;
            try
            {
                connection.Open();
                adapter.Update(ds1, "Propuski");
                adapter.DeleteCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Таблица пропусков успешно сохранена");
                propupdate();
            }
            catch (System.Data.OleDb.OleDbException)
            {
                DialogResult result = MessageBox.Show(@"Возможно вы не сохранили данные о студентах или список дисциплин и пытаетесь сохранить данные о них.
Хотите сохранить ВСЕ несохраненные изменения?","Ошибка базы данных",MessageBoxButtons.OKCancel,MessageBoxIcon.Error);
                if (result == DialogResult.OK)
                {
                    button10_Click(sender, e);
                    button8_Click(sender, e);
                    button9_Click(sender, e);
                    adapter.Update(ds1, "Propuski");
                    adapter.DeleteCommand.ExecuteNonQuery();
                    MessageBox.Show("Таблица пропусков успешно сохранена");
                    propupdate();
                } 
            }
            finally
            {
                connection.Close();
                updatesavedchanges();
                label6.Text = "Несохранённые записи о пропусках отсутствуют";
            }
        }

        private void updatesavedchanges ()
        {
            DateTime time = new DateTime();
            foreach (DataRow dr in ds1.Tables["Propuski"].Rows)
                if ((DateTime)dr["PDate"] > time) time = (DateTime)dr["PDate"];
            label7.Text = "Последняя сохранённая запись за " + time.Date.ToShortDateString();
        }

        private void propupdate()
        {
            updated = false;
            dataGridView3.Rows.Clear();
            foreach (DataRow row in ds1.Tables["Students"].Rows)
            {
                Object[] dr = new Object[] {row["KodStud"],row["SFam"].ToString()+" "+row["SName"].ToString(),0,false};
                foreach (DataRow pos in row.GetChildRows("fio"))
                    if ((dateTimePicker1.Value.Date.Equals((DateTime)pos["PDate"])) && (comboBox1.SelectedValue.Equals(pos["KodDiscip"])) && (pos["Skipped"] != DBNull.Value))
                    {
                        dr[2] = pos["Skipped"];
                        dr[3] = pos["Valid"];
                    }
                dataGridView3.Rows.Add(dr);
            }
            updated = true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            propupdate();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            propupdate();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            readds();
            if ((checkBox1.Checked == false) && (checkedListBox1.CheckedItems.Count == 0))
            { 
                MessageBox.Show("Выберите хотя бы один предмет", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            dataGridView4.Rows.Clear();
            selecteddiscip = null;
            if (!checkBox1.Checked)
            {
                selecteddiscip =  "AND KodDiscip in (";
                foreach (object item in checkedListBox1.CheckedItems)
                {
                    object k = new object();
                    foreach (DataRow row in ds1.Tables["Discip"].Rows)
                        if (item.Equals(row["DName"]))
                        {
                            k = row["KodDiscip"];
                            break;
                        }
                    selecteddiscip += k.ToString() + ",";
                }
                selecteddiscip.Remove(selecteddiscip.LastIndexOf(","));
                selecteddiscip += ")";
            }
            if (numericUpDown1.Value.Equals(0))
            {
                dataGridView4.Columns[2].Visible = false;
                dataGridView4.Columns[4].Visible = false;
            }
            else
            {
                dataGridView4.Columns[2].Visible = true;
                dataGridView4.Columns[4].Visible = true;
            }
            foreach (DataRow row in ds1.Tables["Students"].Rows)
            {
                Object[] dr = new Object[] { row["SFam"].ToString() + " " + row["SName"].ToString(), 0, 0, 0, 0};
                DataTable table = new DataTable();
                table.Columns.Add("KodDiscip");
                table.Columns.Add("KodPrepod");
                table.Columns.Add("KodStud");
                table.Columns.Add("PDate",Type.GetType("System.DateTime"));
                table.Columns.Add("Skipped");
                table.Columns.Add("Valid");
                foreach (DataRow str in row.GetChildRows("fio"))
                    table.ImportRow(str);
                DataRow[] found = table.Select("PDate >= '" + dateTimePicker2.Value.Date.ToString() + "' AND PDate <= '" + dateTimePicker3.Value.Date.ToString() + "'"+selecteddiscip);
                byte k = 0, k1 = 0;
                foreach (DataRow propusk in found) 
                    if (!propusk[4].Equals(DBNull.Value))
                        if (propusk[5].Equals(Boolean.TrueString))
                            k += Convert.ToByte(propusk[4]);
                        else k1 += Convert.ToByte(propusk[4]);
                dr[1] = k;
                dr[3] = k1;
                if (!numericUpDown1.Value.Equals(0))
                {
                    if (k != 0) dr[2] = (int)Math.Round(k / numericUpDown1.Value * 100);
                    if (k1 != 0) dr[4] = (int)Math.Round(k1 / numericUpDown1.Value * 100);
                }
                dataGridView4.Rows.Add(dr);
            }
        }

        private void dataGridView3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (updated)
            {
                DataGridViewRow row = dataGridView3.Rows[e.RowIndex];
                byte result; bool translated = false;
                translated = byte.TryParse(row.Cells[2].Value.ToString(), out result);
                if (!translated)
                {
                    MessageBox.Show(row.Cells[2].Value.ToString() + " не является числом! Измените значение!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (result == 0) return;
                try
                {
                    DataRow dr = ds1.Tables["Propuski"].Select("KodStud = " + row.Cells[0].Value.ToString() + "AND PDate = '" + dateTimePicker1.Value.Date.ToString() + "' AND KodDiscip = " + comboBox1.SelectedValue.ToString())[0];
                    dr["Skipped"] = result;
                    dr["Valid"] = row.Cells[3].Value;
                }
                catch (IndexOutOfRangeException)
                {
                    ds1.Tables["Propuski"].Rows.Add(comboBox1.SelectedValue, DBNull.Value, row.Cells[0].Value, dateTimePicker1.Value.Date, result, row.Cells[3].Value);
                }
                catch (NullReferenceException)
                {
                    MessageBox.Show("Список дисциплин пуст! Добавьте хотя бы одну дисциплину на соответствующей вкладке.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                DateTime time = new DateTime();
                foreach (DataRow dr in ds1.Tables["Propuski"].Rows)
                    if ((DateTime)dr["PDate"] > time) time = (DateTime)dr["PDate"];
                label6.Text = "Последняя несохранённая запись за " + time.Date.ToShortDateString();
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://vk.com/gorodskih");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:gorodskih2008@yandex.ru");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            deleteitem("При удалении студента из списка полностью удаляется вся информация о его пропусках! Вы уверены?", "Students", dataGridView1,"KodStud");
        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            tableupdate(dataGridView5, "Discip");
        }

        private void deleteitem(string message,string table1,DataGridView table2, string parametr)
        {
            DialogResult result = MessageBox.Show(message, "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.OK)
            {
                byte k = 0;
                connection.Open();
                foreach (DataGridViewRow row in table2.SelectedRows)
                {
                    if (!row.Index.Equals(table2.NewRowIndex))
                    {
                        k++;
                        OleDbCommand command = new OleDbCommand("Delete from " + table1 + " Where " + parametr + " = " + row.Cells[0].Value.ToString(), connection);
                        OleDbCommand command2 = new OleDbCommand("Delete from Propuski Where " + parametr + " = " + row.Cells[0].Value.ToString(), connection);
                        command.ExecuteNonQuery();
                        command2.ExecuteNonQuery();
                    }
                }
                connection.Close();
                MessageBox.Show("Успешно удалено "+k+" записей", "Завершено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                readds();
                tableupdate(table2, table1);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            deleteitem("При удалении дисциплины удяляются все прогулы, связанные с ней! Вы уверены?", "Discip", dataGridView5,"KodDiscip");;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) checkedListBox1.Enabled = false;
            else checkedListBox1.Enabled = true;
        }

        private void CellValueChanged(object sender, DataGridViewCellEventArgs e, string table,string parametr)
        {
            if (updated)
            {
                try
                {
                    ds1.Tables[table].Select(parametr + " = " + (sender as DataGridView).Rows[e.RowIndex].Cells[0].Value.ToString())[0][e.ColumnIndex] = (sender as DataGridView).Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                }
                catch (NullReferenceException)
                {
                    byte k = 0;
                    bool finded;
                    do
                    {
                        k++;
                        finded = false;
                        foreach (DataRow row in ds1.Tables[table].Rows)
                            if ((byte)row[parametr] == k) finded = true;
                    } while (finded);
                    DataRow dr = ds1.Tables[table].NewRow();
                    dr[parametr] = k;
                    dr[e.ColumnIndex] = (sender as DataGridView).Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    ds1.Tables[table].Rows.Add(dr);
                    (sender as DataGridView).Rows[e.RowIndex].Cells[0].Value = k;
                }
            }
        }

        private void updatebd(string table ,string selectstring,string messagetext)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter(selectstring, connection);
            adapter.UpdateCommand = new OleDbCommandBuilder(adapter).GetUpdateCommand();
            adapter.DeleteCommand = new OleDbCommandBuilder(adapter).GetDeleteCommand();
            adapter.InsertCommand = new OleDbCommandBuilder(adapter).GetInsertCommand();
            adapter.Update(ds1, table);
            MessageBox.Show(messagetext);
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CellValueChanged(dataGridView1, e, "Students", "KodStud");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            updatebd("Students", "select KodStud,SFam,SName,Phone,Address from Students order by SFam,SName", "Список студентов успешно сохранён");
        }

        private void dataGridView5_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CellValueChanged(dataGridView5, e, "Discip", "KodDiscip");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            updatebd("Discip", "select * from discip order by DName","Список дисциплин успешно сохранён.");
            comboBox1.DataSource = ds1.Tables["Discip"];
            comboBox1.DisplayMember = "DName";
            comboBox1.ValueMember = "KodDiscip";
            checkedListBox1.Items.Clear();
            foreach (DataRow row in ds1.Tables["Discip"].Rows)
                checkedListBox1.Items.Add(row["DName"]);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            updatebd("Prepod", "select * from Prepod order by PFam,PName,POtch","Список преподавателей успешно сохранён");
        }

        private void dataGridView2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CellValueChanged(sender, e, "Prepod", "KodPrepod");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
@"На данной вкладке можно вести журнал посещаемости.
Для того, чтобы отредактировать журнал (добавить, удалить или же изменить количество пропусков), необходимо:
1. Выбрать название дисциплины
2. Выбрать дату, когда была проведена пара
4. Заполнить для каждого студента суммарное количество пропущенных часов по этому предмету за этот день и отметить галочкой, если пропуск был по уважительной причине
5. Нажать на кнопку ""Сохранить изменения"", чтобы данные сохранились на диск.
Внимание: изменения сохраняются только с этой вкладки.", "Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            ds1.Tables["Students"].Rows.Remove(ds1.Tables["Students"].Select("KodStud = "+e.Row.Cells[0].Value.ToString())[0]);
        }

        private void tabPage5_Enter(object sender, EventArgs e)
        {
            checkedListBox1.Items.Clear();
            foreach (DataRow row in ds1.Tables["Discip"].Rows)
                checkedListBox1.Items.Add(row["DName"]);
        }
    }
}
